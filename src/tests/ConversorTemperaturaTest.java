package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import conversor.ConversorTemperatura;


public class ConversorTemperaturaTest {

	ConversorTemperatura meuConversor; 
	
	@Test
	public void testCelsiusParaFahrenheit() {
		meuConversor = new ConversorTemperatura();
		assertEquals(104,meuConversor.celsiusParaFahrenheit());
	}

	public void testCelsiusParaFahrenheitComValorCelsius() {
		meuConversor = new ConversorTemperatura();
		assertEquals(104,meuConversor.celsiusParaFahrenheit(40));
	}

	public void testCelsiusParaFahrenheitComValorCelsiusDouble() {
		meuConversor = new ConversorTemperatura();
		assertEquals(80.6,meuConversor.celsiusParaFahrenheit(27.0));
	}
	
}

package conversor;

public class ConversorTemperatura {

	public int celsiusParaFahrenheit(){
		return 9*40/5+32;
	}
	
	public int celciusParaFahrenheit(int valorCelsius){
		return 9*valorCelsius/5+32;
	}
	
	public double celciusParaFahrenheit(double valorCelsius){
		return 9*valorCelsius/5+32;
	}
	
}



